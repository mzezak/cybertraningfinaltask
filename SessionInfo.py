
class TCPSession:
    def __init__(self):
        self.proto = 'TCP'
        self.dir = None
        self.rx_count = 0
        self.tx_count = 0
        self.loc_ip = None
        self.loc_port = None
        self.rem_ip = None
        self.rem_port = None


class UDPSession:
    def __init__(self):
        self.proto = 'UDP'
        self.dir = None
        self.rx_count = 0
        self.tx_count = 0
        self.loc_ip = None
        self.loc_port = None
        self.rem_ip = None
        self.rem_port = None


# ICMP echo request and echo reply
# The Identifier and Sequence Number can be used by the client to match the reply with the request that caused
# the reply. In practice, most Linux systems use a unique identifier for every ping process, and sequence number
# is an increasing number within that process. Windows uses a fixed identifier, which varies between Windows
# versions, and a sequence number that is only reset at boot time.

class PINGSession:
    def __init__(self):
        self.proto = 'PING'
        self.dir = None
        self.rx_count = 0
        self.tx_count = 0
        self.loc_ip = None
        self.rem_ip = None
        self.id = 0
        self.seq = 0
        self.request = 0
        self.reply = 0

