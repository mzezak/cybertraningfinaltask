from collections import OrderedDict


class LRUDict(OrderedDict):
    """ Limit size, evicting the least recently looked-up key when full """

    def __init__(self, maxsize=128, *args, **kwds):
        self.__maxsize = maxsize
        super().__init__(*args, **kwds)

    def __getitem__(self, key):
        value = super().__getitem__(key)
        self.move_to_end(key)
        return value

    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        if len(self) > self.__maxsize:
            oldest = next(iter(self))
            del self[oldest]


def main():
    '''
    main()
    testing the ordered dict
    '''

    d = LRUDict()
    for i in range(10):
        d[i] = str(i)
    print(d)

    x = next(iter(d))
    print(x)
    d.move_to_end(0)
    x = next(iter(d))
    print(d)

    d.move_to_end(5)
    print(d)

    x = next(iter(d))
    print(x)

    d[100] = '100'
    print(d)
    print(d.get(50))


if __name__ == '__main__':
    main()
