import sqlite3
from datetime import datetime
from os.path import isfile
from os import getcwd
from SessionInfo import TCPSession, UDPSession
import pandas as pd

db_name = 'sniff_' + datetime.now().strftime("%Y%m%d") + '.db'
#print(db_name)

def init_db():
    if not isfile(getcwd() + '\\' + db_name):
        create_db()
    # else, Can add db integrity tests also..... 
 
def create_db():
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    # DataTypes: NULL,INTEGER,REAL,TEXT,BLOB
    cur.execute("""
        CREATE TABLE sessions(
            local_ip text NOT NULL,
            local_port text NOT NULL,
            remote_ip text NOT NULL,
            remote_port text NOT NULL,
            protocol text NOT NULL,
            init_direction text,
            recieved real,
            transmir real,
            start_time text,
            end_time text,
            PRIMARY KEY (local_ip,local_port,remote_ip,remote_port,protocol)
        )
    """)
    conn.commit()
    conn.close()

def add_session(session=None):
    if type(session) is TCPSession or UDPSession:
        par = []
        par.append(session.loc_ip)
        par.append(session.loc_port)
        par.append(session.rem_ip)
        par.append(session.rem_port)
        if type(session) is TCPSession:
             par.append('TCP')
        else:
            par.append('UDP')
        par.append(session.dir)
        par.append(session.rx_count)
        par.append(session.tx_count)
        par.append(datetime.now().strftime("%H:%M:%S"))
        par.append(datetime.now().strftime("%H:%M:%S"))
        conn = sqlite3.connect(db_name)
        cur = conn.cursor()
        cur.execute("INSERT INTO sessions VALUES(?,?,?,?,?,?,?,?,?,?)",tuple(par))
        conn.commit()
        conn.close()
    else:
        print('None or invalid Session passed!')

def update_session(session=None):
    if type(session) is TCPSession or UDPSession:
        par = []
        par.append(session.rx_count)
        par.append(session.tx_count)
        par.append(datetime.now().strftime("%H:%M:%S"))

        par.append(session.loc_ip)
        par.append(session.loc_port)
        par.append(session.rem_ip)
        par.append(session.rem_port)
        if type(session) is TCPSession:
             par.append('TCP')
        else:
            par.append('UDP')       
        conn = sqlite3.connect(db_name)
        cur = conn.cursor()
        cur.execute(""" UPDATE sessions SET recieved=?, transmir=?, end_time=? 
                        WHERE local_ip=? 
                        AND local_port=? 
                        AND remote_ip=? 
                        AND remote_port=? 
                        AND protocol=?""",tuple(par))
        conn.commit()
        conn.close()
    else:
        print('None or invalid Session passed!')

def report0():
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    cur.execute(""" SELECT * FROM sessions 
                    ORDER BY start_time ASC""")
    result = cur.fetchall() 
    conn.commit()
    conn.close()
    print('All Sessions:')
    return result

def report1():
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    cur.execute(""" SELECT remote_ip, count(remote_ip) 
                    FROM sessions 
                    GROUP BY remote_ip 
                    ORDER BY 2 DESC""")
    result = cur.fetchall() 
    conn.commit()
    conn.close()
    print('Remote IP with most Sessions:')
    return result

def report2():
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    cur.execute(""" SELECT remote_ip, sum(recieved) FROM sessions 
                    GROUP BY remote_ip 
                    ORDER BY 2 DESC""")
    result = cur.fetchall() 
    conn.commit()
    conn.close()
    print('Remote IP by revieved data size:')
    return result

def report3():
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    cur.execute(""" SELECT remote_ip, sum(transmir) FROM sessions 
                    GROUP BY remote_ip 
                    ORDER BY 2 DESC""")
    result = cur.fetchall() 
    conn.commit()
    conn.close()
    print('Remote IP by transmited data size:')
    return result

def report4():
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    cur.execute(""" SELECT remote_port, count(remote_port) FROM sessions 
                    GROUP BY remote_port 
                    ORDER BY 2 DESC""")
    result = cur.fetchall() 
    conn.commit()
    conn.close()
    print('Most Popular remote port:')
    return result

def report5():
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    cur.execute(""" SELECT protocol, count(protocol) FROM sessions 
                    GROUP BY 1
                    ORDER BY 2 DESC""")
    result = cur.fetchall() 
    conn.commit()
    conn.close()
    print('TCP vs. UDP:')
    return result

def main():
    init_db()
    # s = TCPSession()
    # s.dir = 'outbound'
    # s.loc_ip = '1.1.1.1'
    # s.loc_port = '1717'
    # s.rem_ip = '2.2.2.2'
    # s.rem_port = '7171'
    # add_session(s)

    # s = UDPSession()
    # s.dir = 'inbound'
    # s.loc_ip = '3.3.3.3'
    # s.loc_port = '5555'
    # s.rem_ip = '4.4.4.4'
    # s.rem_port = '6666'
    # add_session(s)

    print('===============================')
    rep0 = report0()
    print('===============================')
    print(pd.DataFrame(rep0))

    print('===============================')
    rep1 = report1()
    print('===============================')
    print(pd.DataFrame(rep1,columns={'IP Address', 'Calls'}))
    
    print('===============================')
    rep2 = report2()
    print('===============================')
    print(pd.DataFrame(rep2,columns={'IP Address', 'Data Recieved'}))

    print('===============================')
    rep3 = report3()
    print('===============================')
    print(pd.DataFrame(rep3,columns={'IP Address', 'Data Transmited'}))

    print('===============================')
    rep4 = report4()
    print('===============================')
    print(pd.DataFrame(rep4,columns={'Remote Port', 'Calls'}))

    print('===============================')
    rep5 = report5()
    print('===============================')
    print(pd.DataFrame(rep5,columns={'Protocol', 'Sessions'}))


if __name__ == '__main__':
    main()