from scapy.all import *
import scapy.layers.http
from scapy.layers.dns import DNSQR, DNS
from scapy.layers.inet import IP, TCP, ICMP, UDP
from scapy.layers.l2 import ARP
from scapy.layers.inet6 import IPv6

from LRUDict import LRUDict
from SessionInfo import TCPSession, UDPSession, PINGSession
from SqlSniffs import init_db, add_session, update_session

sessions = LRUDict(maxsize=4096)
local_ip = IP().src
pcount = 0

UPDATE_INTERVAL = 1
IGNOR_TCP = False
IGNOR_UDP = False
IGNOR_PING = False
IGNORE_DB = False


def handle_tcp(p, pcnt):
    global sessions

    if IGNOR_TCP:
        return 'Block TCP'

    proto = p[IP].proto
    src_ip = p[IP].src
    dst_ip = p[IP].dst
    sport = p[TCP].sport
    dport = p[TCP].dport

    # # if dst_ip != '104.16.130.3' and src_ip != '104.16.131.3': # !!!!!!!!!!!!!!!!!!!!!!!!!!!
    # if (dst_ip or src_ip) in ('104.16.130.3', '104.16.131.3'):
    #     pass
    # else:
    #     return 'not ynet'

    if local_ip == src_ip:
        hkey = (src_ip, sport, dst_ip, dport, proto)
        direction = 'outbound'
    elif local_ip == dst_ip:
        hkey = (dst_ip, dport, src_ip, sport, proto)
        direction = 'inbound'
    else:
        return f'{pcnt}: TCP, unknown local ip'

    session = sessions.get(hkey)
    if session is None:
        stcp = TCPSession()
        if direction == 'outbound':
            stcp.dir = 'outbound'
            stcp.loc_ip = src_ip
            stcp.loc_port = sport
            stcp.rem_ip = dst_ip
            stcp.rem_port = dport
        else:
            stcp.dir = 'inbound'
            stcp.loc_ip = dst_ip
            stcp.loc_port = dport
            stcp.rem_ip = src_ip
            stcp.rem_port = sport

        sessions[hkey] = stcp

        # INSERT SQL SESSION HERE !!!!!
        if not IGNORE_DB:  
            init_db()   
            add_session(sessions[hkey])

    if direction == 'inbound':
        sessions[hkey].rx_count += len(p[TCP].payload)
        # print(sessions[hkey].rx_count)
    else:
        sessions[hkey].tx_count += len(p[TCP].payload)
        # print(sessions[hkey].rx_count)

    # UPDATE SQL SESSION HERE - count and end time !!!!!
    if not IGNORE_DB and pcnt%UPDATE_INTERVAL==0:  
            update_session(sessions[hkey])

    return f'{pcnt}: TCP'


def handle_udp(p, pcnt):
    global sessions

    if IGNOR_UDP:
        return 'Block UDP'

    proto = p[IP].proto
    src_ip = p[IP].src
    dst_ip = p[IP].dst
    sport = p[UDP].sport
    dport = p[UDP].dport

    if local_ip == src_ip:
        hkey = (src_ip, sport, dst_ip, dport, proto)
        direction = 'outbound'
    elif local_ip == dst_ip:
        hkey = (dst_ip, dport, src_ip, sport, proto)
        direction = 'inbound'
    else:
        return f'{pcnt}: UDP, unknown local ip'

    session = sessions.get(hkey)
    if session is None:
        sudp = UDPSession()
        if direction == 'outbound':
            sudp.dir = 'outbound'
            sudp.loc_ip = src_ip
            sudp.loc_port = sport
            sudp.rem_ip = dst_ip
            sudp.rem_port = dport
        else:
            sudp.dir = 'inbound'
            sudp.loc_ip = dst_ip
            sudp.loc_port = dport
            sudp.rem_ip = src_ip
            sudp.rem_port = sport

        # INSERT SQL SESSION HERE !!!!!
        sessions[hkey] = sudp

        if not IGNORE_DB:  
            init_db()   
            add_session(sessions[hkey])

    if direction == 'inbound':
        sessions[hkey].rx_count += len(p[UDP].payload)
    else:
        sessions[hkey].tx_count += len(p[UDP].payload)

    # UPDATE SQL SESSION HERE !!!!!
    if not IGNORE_DB and pcnt%UPDATE_INTERVAL==0:  
            update_session(sessions[hkey])

    return f'{pcnt}: UDP'


def handle_ping(p, pcnt):
    global sessions

    if IGNOR_PING:
        return 'Block PING'

    if p[ICMP].type not in (0, 8):
        return f'{pcnt}: Only ICMP echo request, reply supported'

    proto = p[IP].proto
    src_ip = p[IP].src
    dst_ip = p[IP].dst
    id = p[ICMP].id
    seq = p[ICMP].seq

    if local_ip == src_ip:
        hkey = (src_ip, dst_ip, id, seq, proto)
        direction = 'outbound'
    elif local_ip == dst_ip:
        hkey = (dst_ip, src_ip, id, seq, proto)
        direction = 'inbound'
    else:
        return f'{pcnt}: ICMP, unknown local ip'

    session = sessions.get(hkey)
    if session is None:
        sping = PINGSession()
        sping.id = id
        sping.seq = seq
        if direction == 'outbound':
            sping.dir = 'outbound'
            sping.loc_ip = src_ip
            sping.rem_ip = dst_ip
        else:
            sping.dir = 'inbound'
            sping.loc_ip = dst_ip
            sping.rem_ip = src_ip

        sessions[hkey] = sping
    if direction == 'inbound':
        sessions[hkey].rx_count += len(p[ICMP].payload)
    else:
        sessions[hkey].tx_count += len(p[ICMP].payload)

    if p[ICMP].type == 8:
        sessions[hkey].request += 1
        ptype = 'request'
    else:
        sessions[hkey].reply += 1
        ptype = 'reply'

    return f'{pcnt}: PING {ptype}'


def callback(p):
    global pcount

    pcount += 1

    if IPv6 in p:
        return 'Not supporting IPv6'
    elif TCP in p:
        return handle_tcp(p, pcount)
    elif UDP in p:
        return handle_udp(p, pcount)
    elif ICMP in p:
        return handle_ping(p, pcount)

    return f'{pcount}: Unknown packet'


def main():
    sniff(prn=callback, store=False, filter='tcp or udp or icmp')


if __name__ == '__main__':
    main()
